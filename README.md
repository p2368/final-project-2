<h1>Final Project 2 PKS DS</h1>

## Kelompok 11

## Anggota Kelompok

- Mohamad Fa'ul Janaqi
- Anton Kartono (Mengundurkan Diri)
- Agus Jauhari (Pindah Kelompok)

## Tema Project

Aplikasi Kas Mesjid Sederhana.
User harus login ke sistem terlebih dahulu untuk menggunakan aplikasi ini. Jika belum memiliki akun, maka harus registrasi terlebih dahulu. Jika sudah berhasil registrasi, user akan otomatis memiliki role sebagai 'common'. User dengan role common ini hanya dapat melihat daftar transaksi dan mengupdate profile saja. Untuk menambah transaksi harus menggunakan akun dengan role 'admin'. User dengan role admin dapat mengubah user dengan role common menjadi admin. User dengan role admin dapat mengubah dan menghapus semua user yang ada di dalam sistem. Hanya user dengan role admin yang dapat melakukan CRUD data transaksi.

NB :

- lakukan php artisan migrate
- lakukan php artisan db:seed agar tipe transaksi, role, dan user admin masuk ke data base
- jika melakukan uji coba registrasi user, harap menggunakan akun email yang aktif, karena hostingnya memblokir selama 1 jam jika mengirim ke email yang tidak valid sebanyak 5 kali dalam 1 jam.

## ERD

<p align="center"><a href="https://kasmesjid.cowayjkt.id" target="_blank"><img src="https://cowayjkt.id/assets/kas-mesjid.png" width="400"></a></p>

## Link Video

- Link Demo Aplikasi: https://www.youtube.com/watch?v=RxmgJCiVtCg
- Link Screenshot Aplikasi: https://drive.google.com/drive/folders/1XeFCVC4dRh0CS0q8dHVCqhDn9053ctCR?usp=sharing
- Link Dokumentasi API: https://documenter.getpostman.com/view/14612045/UVR5q8cC
