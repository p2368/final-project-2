import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "about" */ '../views/Home.vue')
  },
  {
    path: '/transaction',
    name: 'Transaction',
    component: () => import(/* webpackChunkName: "about" */ '../views/Transaction.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/add-transaction',
    name: 'Add Transaction',
    component: () => import(/* webpackChunkName: "about" */ '../views/AddTransaction.vue'),
    meta: { requiresAuth: true, is_admin: true }
  },
  {
    path: '/edit-transaction/:id',
    name: 'Edit Transaction',
    component: () => import(/* webpackChunkName: "about" */ '../views/EditTransaction.vue'),
    meta: { requiresAuth: true, is_admin: true }
  },
  {
    path: '/profile',
    name: 'Update Profile',
    component: () => import(/* webpackChunkName: "about" */ '../views/Profile.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/users',
    name: 'Daftar Users',
    component: () => import(/* webpackChunkName: "about" */ '../views/User.vue'),
    meta: { requiresAuth: true, is_admin: true }
  },
  {
    path: '/user/:id',
    name: 'Edit User',
    component: () => import(/* webpackChunkName: "about" */ '../views/EditUser.vue'),
    meta: { requiresAuth: true, is_admin: true }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {

    if (!store.getters['auth/requiresAuth']) {
      alert('Harus login terlebih dahulu')
      next({ name: 'Home' })
    } else if (to.matched.some(record => record.meta.is_admin)) {
      if (!store.getters['auth/isAdmin']) {
        alert('Anda bukan administrator !')
        next({ name: 'Home' })
      } else {
        next()
      }
    } else {
      next()
    }
  } else {
    next()
  }
})



export default router
