import axios from "axios"

export default {
    namespaced: true,

    state: {
        token: '',
        user: {},
        requiresAuth: '',
        isAdmin: ''
    },

    mutations: {
        setToken: (state, payload) => {
            state.token = payload
        },

        setUser: (state, payload) => {
            state.user = payload
        },

        setRequiresAuth: (state, payload) => {
            state.requiresAuth = payload
        },

        setIsAdmin: (state, payload) => {
            state.isAdmin = payload
        }
    },

    actions: {
        setToken: ({ commit, dispatch }, payload) => {
            commit('setToken', payload)
            dispatch('checkToken', payload)
        },

        checkToken: ({ commit }, payload) => {
            let config = {
                method: 'post',
                url: 'https://kas-mesjid.cowayjkt.id/api/auth/check',
                headers: {
                    'Authorization': 'Bearer ' + payload
                }
            }

            axios(config)
                .then((response) => {
                    commit('setUser', response.data)
                    commit('setRequiresAuth', 'loggedIn')

                    if (response.data.role.name === 'admin') {
                        commit('setIsAdmin', 'admin')
                    }
                })
                .catch(() => {
                    commit('setUser', {})
                    commit('setToken', '')
                })
        },

        setUser: ({ commit }, payload) => {
            commit('setUser', payload)
        },

        setRequiresAuth: ({ commit }, payload) => {
            commit('setRequiresAuth', payload)
        },

        setIsAdmin: ({ commit }, payload) => {
            commit('setIsAdmin', payload)
        }
    },

    getters: {
        user: state => state.user,
        token: state => state.token,
        guest: state => Object.keys(state.user).length === 0,
        requiresAuth: state => state.requiresAuth,
        isAdmin: state => state.isAdmin
    }
}