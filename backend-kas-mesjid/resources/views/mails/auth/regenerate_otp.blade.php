<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kode OTP Baru | {{ config('app.name') }}</title>
</head>

<body>
    <h1>Halo <strong>{{ $otp_code->user->name }}</strong></h1>
    <p>
        Berikut ini adalah Kode OTP baru Anda :
    </p>
    <br>
    <h2>
        {{ $otp_code->otp }}
    </h2>
    <br>
    <p>
        <em>
            Kode OTP ini hanya berlaku 30 menit. <strong>Jangan pernah berikan kode ini kepada siapapun.</strong>
        </em>
    </p>
    <hr>
    <p>
        <small>
            Admin Kas Mesjid
        </small>
    </p>
</body>

</html>