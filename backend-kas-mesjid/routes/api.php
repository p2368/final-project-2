<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::group([
//     'namespace' => 'API'
// ], function () {
//     Route::apiResource('/transaction', 'TransactionController')->only(['index', 'show']);
// });

Route::group([
    'namespace' => 'API',
    'middleware' => 'auth:api'
], function () {
    Route::apiResource('/transaction', 'TransactionController');
    Route::apiResource('/trx-type', 'TrxTypeController');
    Route::apiResource('/role', 'RoleController');
    Route::apiResource('/user', 'UserController')->only(['index', 'show', 'update', 'destroy']);
});

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth'
], function () {
    Route::post('register', 'RegisterController')->name('auth.register');
    Route::post('regenerate-otp-code', 'RegenerateOtpCodeController')->name('auth.regenerate_otp_code');
    Route::post('verification', 'VerificationController')->name('auth.verification');
    Route::post('update-password', 'UpdatePasswordController')->name('auth.update_password');
    Route::post('login', 'LoginController')->name('auth.login');
    Route::post('logout', 'LogoutController')->name('auth.logout');
});

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth',
    'middleware' => 'auth:api'
], function () {
    Route::put('update-profile', 'UpdateProfileController')->name('auth.update_profile');
    Route::post('check', 'CheckController')->name('auth.check');
});
