<?php

use App\Role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleId = Role::where('name', 'admin')->first()->id;

        $data = [
            'id' => Uuid::uuid4()->toString(),
            'name' => 'Administrator',
            'email' => 'admin@admin.com',
            'photo' => null,
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('123456'),
            'role_id' => $roleId,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ];

        // User::create($data);

        DB::table('users')->insert($data);
    }
}
