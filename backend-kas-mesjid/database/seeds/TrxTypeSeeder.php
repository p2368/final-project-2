<?php

use App\TrxType;
use Illuminate\Database\Seeder;

class TrxTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'debit'],
            ['name' => 'kredit']
        ];

        foreach ($data as $datum) {
            TrxType::create($datum);
        }
    }
}
