<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('role')->get();

        return response()->json([
            'success' => true,
            'message' => 'Users List',
            'data' => $users
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return response()->json([
            'success' => true,
            'message' => 'User Detail',
            'data' => $user
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'email',
            'photo' => 'image|mimes:png,jpg,jpeg,gif|max:2048'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::findOrFail($user->id);

        if ($user) {
            if ($request->has('photo')) {
                $imgName = time() . '.' . $request->photo->extension();
                $request->photo->move(public_path('img/photo'), $imgName);

                if ($user->photo) {
                    $imgTmp = $user->photo;
                    File::delete('img/photo/' . $imgTmp);
                }

                $user->photo = $imgName;
            }

            if ($request->has('email')) {

                if ($user->email != $request->email) {
                    $user->email = $request->email;
                }
            }

            if ($request->has('role_id')) {
                if ($user->role_id != $request->role_id) {
                    $user->role_id = $request->role_id;
                }
            }

            $user->name = $request->name;

            $user->update();

            return response()->json([
                'success' => true,
                'message' => 'User has been updated',
                'data' => $user
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'User is not found'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        if ($user) {
            $loggedIn = auth()->user();

            if ($user->id == $loggedIn->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'You can not delete your self'
                ], 403);
            }

            if ($user->photo) {
                $path = 'img/photo/';
                $imageName = $user->photo;
                File::delete($path . $imageName);
            }

            $user->delete();

            return response()->json([
                'success' => true,
                'message' => 'User has been deleted'
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'User is not found'
        ], 404);
    }
}
