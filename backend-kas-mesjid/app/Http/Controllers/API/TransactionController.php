<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::with('trxType')->orderByDesc('created_at')->get();

        return response()->json([
            'success' => true,
            'message' => 'Transaction Data',
            'data' => $transactions
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nominal' => 'required',
            'keterangan' => 'required',
            'trx_type_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = auth()->user();

        $transaction = Transaction::create([
            'nominal' => $request->nominal,
            'keterangan' => $request->keterangan,
            'user_id' => $user->id,
            'trx_type_id' => $request->trx_type_id
        ]);

        if ($transaction) {
            return response()->json([
                'success' => true,
                'message' => 'New transaction created',
                'data' => $transaction
            ], 201);
        }

        return response()->json([
            'success' => false,
            'message' => 'Transaction failed to save'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction::with('trxType')->findOrFail($id);

        return response()->json([
            'success' => true,
            'message' => 'Transaction detail',
            'data' => $transaction
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        $validator = Validator::make($request->all(), [
            'nominal' => 'required',
            'keterangan' => 'required',
            'trx_type_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $transaction = Transaction::findOrFail($transaction->id);

        if ($transaction) {
            $user = auth()->user();

            if ($transaction->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'You are not authorized for this transaction'
                ], 403);
            }

            $transaction->update([
                'nominal' => $request->nominal,
                'keterangan' => $request->keterangan,
                'trx_type_id' => $request->trx_type_id
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Transaction has been updated',
                'data' => $transaction
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Transaction is not found'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = Transaction::findOrFail($id);

        if ($transaction) {
            $user = auth()->user();

            if ($transaction->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'You are not authorized for this transaction'
                ], 403);
            }

            $transaction->delete();

            return response()->json([
                'success' => true,
                'message' => 'Transaction was deleted'
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Transaction is not found'
        ], 404);
    }
}
