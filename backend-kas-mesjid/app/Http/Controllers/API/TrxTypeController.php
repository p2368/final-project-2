<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\TrxType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TrxTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trxTypes = TrxType::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Transaction Type List',
            'data' => $trxTypes
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $trxType = TrxType::create([
            'name' => $request->name
        ]);

        if ($trxType) {
            return response()->json([
                'success' => true,
                'message' => 'New transaction type was created',
                'data' => $trxType
            ], 201);
        }

        return response()->json([
            'success' => false,
            'message' => 'New transaction type failed to save'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $trxType = TrxType::findOrFail($id);

        return response()->json([
            'success' => true,
            'message' => 'Transaction Type Detail',
            'data' => $trxType
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TrxType $trxType)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $trxType = TrxType::findOrFail($trxType->id);

        if ($trxType) {
            $trxType->update([
                'name' => $request->name
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Transaction type has been updated',
                'data' => $trxType
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Transaction type is not found'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trxType = TrxType::findOrFail($id);

        if ($trxType) {
            $trxType->delete();

            return response()->json([
                'success' => true,
                'message' => 'Transaction type has been deleted'
            ], 202);
        }

        return response()->json([
            'success' => false,
            'message' => 'Transaction type is not found'
        ], 404);
    }
}
