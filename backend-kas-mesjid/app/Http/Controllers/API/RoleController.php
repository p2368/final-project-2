<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Roles List',
            'data' => $roles
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $role = Role::create([
            'name' => $request->name
        ]);

        if ($role) {
            return response()->json([
                'success' => true,
                'message' => 'New role was created',
                'data' => $role
            ], 201);
        }

        return response()->json([
            'success' => false,
            'message' => 'New role failed to save'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::findOrfail($id);

        return response()->json([
            'success' => true,
            'message' => 'Role Detail',
            'data' => $role
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $role = Role::findOrFail($role->id);

        if ($role) {
            $role->update([
                'name' => $request->name
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Role has been updated',
                'data' => $role
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Role is not found'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);

        if ($role) {
            $role->delete();

            return response()->json([
                'success' => true,
                'message' => 'Role has been deleted'
            ], 202);
        }

        return response()->json([
            'success' => false,
            'messsage' => 'Role is not found'
        ], 404);
    }
}
