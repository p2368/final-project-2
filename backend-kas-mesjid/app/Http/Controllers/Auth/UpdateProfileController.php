<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class UpdateProfileController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'name'  => 'required',
            'photo' => 'image|mimes:png,jpg,jpeg,gif|max:2048'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::findOrFail(auth()->user()->id);

        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User is not found'
            ], 404);
        }

        if ($request->has('photo')) {
            $imgName = time() . '.' . $request->photo->extension();
            $request->photo->move(public_path('img/photo'), $imgName);

            if ($user->photo) {
                $imgTmp = $user->photo;
                File::delete('img/photo/' . $imgTmp);
            }

            $user->update([
                'name' => $request->name,
                'photo' => $imgName
            ]);

            return response()->json([
                'success' => true,
                'message' => 'User has been updated',
                'data' => $user
            ], 200);
        }

        $user->update([
            'name' => $request->name
        ]);

        return response()->json([
            'success' => true,
            'message' => 'User has been updated',
            'data' => $user
        ], 200);
    }
}
