<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;

class CheckController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $user = User::with('role')->where('id', auth()->user()->id)->first();

        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User is not found'
            ], 400);
        }

        return response()->json($user);
    }
}
